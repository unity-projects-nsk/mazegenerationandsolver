﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderController : MonoBehaviour {

    public Slider widthSlider;
    public Slider heightSlider;
    public TextMeshProUGUI widthText;
    public TextMeshProUGUI heightText;
	
	// Update is called once per frame
	void Update () {
        widthText.text = widthSlider.value.ToString();
        heightText.text = heightSlider.value.ToString();
	}
}
