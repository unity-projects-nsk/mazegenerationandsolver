﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour {

    public MazeGeneration mg;
    public Button DFS;
    public Button BFS;
	
	// Update is called once per frame
	void Update () {
		if (mg.isDFSearched)
        {
            DFS.interactable = false;
        } else
        {
            DFS.interactable = true;
        }

        if (mg.isBFSearched)
        {
            BFS.interactable = false;
        }
        else
        {
            BFS.interactable = true;
        }

    }
}
