﻿using System.Collections.Generic;

public class Stack<E> : IStack<E> {
    private List<E> stack;
    private int size;

    public Stack()
    {
        stack = new List<E>();
        size = 0;
    }

    public int Size()
    {
        return size;
    }

    public bool IsEmpty()
    {
        return (size == 0);
    }

    public E Pop()
    {
        if (size == 0)
            return default(E);
        E temp = stack[0];
        stack.RemoveAt(0);
        size--;
        return temp;
    }

    public void Push(E e)
    {
        stack.Insert(0, e);
        size++;
    }

    public E Top()
    {
        if (size == 0)
            return default(E);
        return stack[0];
    }
}
