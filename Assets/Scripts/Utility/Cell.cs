﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour {
    public Image cellImage;
    public CellFormat format;
    public int MazeWidth;
    public int MazeHeight;

    public int X { get; set; }
    public int Y { get; set; }
    public bool WasVisited { get; set; }
    public bool WasQueued { get; set; }
    public bool IsBlocked { get; set; }
    public bool IsStart { get; set; }
    public bool IsEnd { get; set; }
    public bool IsPath { get; set; }
    public Cell NorthCell { get; set; }
    public Cell EastCell { get; set; }
    public Cell WestCell { get; set; }
    public Cell SouthCell { get; set; }
    public int Distance { get; set; }

    public void SetMazeSize(int width, int height)
    {
        this.MazeWidth = width;
        this.MazeHeight = height;
    }

    public void FormatCell()
    {
        if (IsBlocked)
            cellImage.color = format.wallColor;
        else if (IsStart)
            cellImage.color = format.startColor;
        else if (IsEnd)
            cellImage.color = format.endColor;
        else if (IsPath)
            cellImage.color = format.pathColor;
        else if (WasVisited)
            cellImage.color = format.visitedColor;
        else
            cellImage.color = format.openColor;
    }

    public Cell GetNorth(Cell cell, List<Cell> cells)
    {
        if (cell.Y + 1 > cell.MazeHeight)
            return null;
        else
            return FindCell(cells, cell.X, cell.Y + 1);
    }
    public Cell GetWest(Cell cell, List<Cell> cells)
    {
        if (cell.X - 1 < 0)
            return null;
        else
            return FindCell(cells, cell.X - 1, cell.Y);
    }
    public Cell GetEast(Cell cell, List<Cell> cells)
    {
        if (cell.X + 1 > cell.MazeWidth)
            return null;
        else
            return FindCell(cells, cell.X + 1, cell.Y);
    }
    public Cell GetSouth(Cell cell, List<Cell> cells)
    {
        if (cell.Y - 1 < 0)
            return null;
        else
            return FindCell(cells, cell.X, cell.Y - 1);
    }

    public Cell FindCell(List<Cell> cells, int x, int y)
    {
        for (int i = 0; i < cells.Count; i++)
        {
            Cell tempCell = cells[i];
            if (tempCell.X == x && tempCell.Y == y)
                return cells[i];
        } return null;
    }
}
