﻿public interface IStack<E>
{
    int Size();
    bool IsEmpty();
    void Push(E e);
    E Top();
    E Pop();
}
